# maven docker image

ESS maven docker image based on the [official docker image](https://hub.docker.com/_/maven/).

It includes a settings.xml specific to ESS and adds a maven user (uid: 1000).
The `$MAVEN_CONFIG` variable is set to `/home/maven/.m2`.

There is no Dockerfile in the master branch.
You should create one branch per openjdk release.

## How to use this image

You can run a Maven project by using the Maven Docker image directly, passing a Maven command to docker run:

```
$ docker run -it --rm -v "$(pwd)":/src -w /src registry.esss.lu.se/ics-docker/maven:3.5-4-jdk-11 mvn clean install
```

To use a local cache, you can mount your local .m2 directory to  `/home/maven/.m2`.

```
$ docker run -it --rm -v "$HOME/.m2":/home/maven/.m2 -v "$(pwd)":/src -w /src registry.esss.lu.se/ics-docker/maven:3.5-4-jdk-11 mvn clean package
```
